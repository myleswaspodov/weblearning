<!--outter-wp-->
          <div class="outter-wp">
            <!--sub-heard-part-->
            <div class="sub-heard-part">
              <ol class="breadcrumb m-b-0">
                <li><a href="<?php echo site_url('mhs_home')?>">Home</a></li>
                <li class="active">Pre Test</li>
              </ol>
            </div>
            <hr>  
            <div class="graph-visual tables-main">
                <div class="graph">
                  <div class="block-page">
                    <p>
                      <center><h1 class="inner-tittle">Post Test Sudah Dikerjakan</h1><br>
                      <!-- <a href="<?php echo site_url('mhs_course')?>"><button type="button" class="btn btn-warning"><i class="lnr lnr-arrow-left-circle"></i> Kembali</button></a></center> -->
                    </p>
                  </div>
                </div>
            </div>

            <?php if (!empty($soal)) { ?>
            <div class="graph-visual tables-main">
                <div class="graph">
                  <div class="block-page">
                    <p>
                    <div>
                    <?php foreach ($soal as $key => $value) { ?>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="col-md-1">
                            <?php if ($value->kunci == $value->answer) { ?>
                              <i class="fa fa-check"></i>
                            <?php } else {?>
                              <i class="fa fa-times"></i>
                            <?php } ?>

                            <?php echo $key+1 ?>.
                          </div>
                          <div class="col-md-11">
                            <?php echo $value->soal ?>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="col-md-1">
                            <?php $opsi = unserialize($value->opsi); ?>
                          </div>
                          <div class="col-md-11">
                            <?php foreach ($opsi as $op => $tion) { ?>
                               <input disabled type="radio" <?php if ($op == $value->answer) { ?>
                                 checked
                               <?php } ?> >  <?php echo strtoupper($op).". ".$tion ?><br>
                            <?php } ?>
                            
                            <?php if ($value->kunci != $value->answer) { ?>
                              <small style="color: red; font-style: italic;">Kunci Jawaban: <?php echo strtoupper($value->kunci) ?> </small>
                              <br>
                            <?php } ?>
                              <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal<?php echo $key ?>">Pembahasan</button>
                            <br><br>
                          </div>
                        </div>

                        <!-- modal pembahasan -->
                        <div class="modal fade" id="myModal<?php echo $key ?>" role="dialog">
                          <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Pembahasan Soal No. <?php echo $key+1 ?> </h4>
                              </div>
                              <div class="modal-body">
                                <div class="row">
                                  <div class="col-md-12">
                                    <?php echo $value->pembahasan ?>
                                  </div>
                                </div>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              </div>
                            </div>
                            
                          </div>
                        </div>
                        
                      </div>
                    <?php } ?>
                    </div>
                    </p>
                  </div>
                </div>
            </div>
            <?php } ?>    
            <!--//graph-visual-->
          </div>
        </div>
      </div>
      <!--Konten Utama