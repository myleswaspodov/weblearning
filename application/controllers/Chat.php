<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Chat extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
	    $this->load->model('SecurityModel');
		$this->load->model('M_login');
		$this->load->model('M_global');
		$this->load->model('M_chitchat');

		$this->load->helper('tglindo_helper');	   
	}
	
	function index()
	{
		if ($this->session->userdata('level') == 'mhs') {
			$data = $this->prepareIndexMhs();
			$this->load->view('mhslayout/wrapper',$data);
		}

		if ($this->session->userdata('level') == 'dosen') {
			$data = $this->prepareIndexDosen();
			$this->load->view('dosenlayout/wrapper',$data);
		}
	}

	function prepareIndexMhs($recipient=null, $username=null)
	{
		$data = [
			'title'     =>'Chat',
			'isi'       =>'mhspages/chat/index',
			'profil'    => $this->m_global->get_data_all('mahasiswa', null, ['nim'=> $this->session->userdata('username')]),
			'recipient' => $this->getListRecipientMhs($username),
			'chat'		=> [],
			'to'		=> "",
			'from' 		=> $this->session->userdata('usr_id'),
			'sendTo'	=> ""
		];

		if ($data['recipient']) {
			$data['chat']   = $this->M_chitchat->listChat($data['recipient'][0]['usr_id'], $this->session->userdata('usr_id'));
			$data['to']     = $data['recipient'][0]['usr_id'];
			$data['sendTo'] = $data['recipient'][0]['username'];
		}

		if (!is_null($recipient)) {
			$data['chat']   = $this->M_chitchat->listChat($recipient->usr_id, $this->session->userdata('usr_id'));
			$data['sendTo'] = $recipient->username;
			$data['to']     = $recipient->usr_id;
		}

		return $data;
	}

	function prepareIndexDosen($recipient=null, $username=null)
	{
		$data = [
			'title'     =>'Chat',
			'isi'       =>'dosenpages/chat/index',
			'profil'    => $this->m_global->get_data_all('dosen', null, ['kd_dosen'=>$this->session->userdata('username')]),
			'recipient' => $this->getListRecipientDosen($username),
			'chat'		=> [],
			'to'		=> "",
			'from' 		=> $this->session->userdata('usr_id'),
			'sendTo'	=> ""
		];

		if ($data['recipient']) {
			$data['chat']   = $this->M_chitchat->listChat($data['recipient'][0]['usr_id'], $this->session->userdata('usr_id'));
			$data['to']     = $data['recipient'][0]['usr_id'];
			$data['sendTo'] = $data['recipient'][0]['username'];
		}

		if (!is_null($recipient)) {
			$data['chat']   = $this->M_chitchat->listChat($recipient->usr_id, $this->session->userdata('usr_id'));
			$data['sendTo'] = $recipient->username;
			$data['to']     = $recipient->usr_id;
		}

		return $data;
	}

	function to($username)
	{
		if ($recipient = $this->checkValidRecipient($username)) {
			if ($this->session->userdata('level') == 'mhs') {
				$data         = $this->prepareIndexMhs($recipient[0], $username);
				return $this->load->view('mhslayout/wrapper',$data);
			}

			if ($this->session->userdata('level') == 'dosen') {
				$data         = $this->prepareIndexDosen($recipient[0], $username);
				return $this->load->view('dosenlayout/wrapper',$data);
			}
		}

		redirect('home');
	}

	function checkValidRecipient($username)
	{
		if ($check = $this->m_global->get_data_all('user', NULL, ['username'=>$username])) {
			if ($check[0]->level != $this->session->userdata('level')) {
				return $check;
			}
		}

		return false;
	}

	function getListRecipientMhs($flag = NULL)
	{
		$dosen = $this->m_global->get_data_all('dosen', [
			[ 'mengajar', 'mengajar.kd_dosen = dosen.kd_dosen', 'RIGHT'], 
			[ 'user', 'user.username = dosen.kd_dosen', 'LEFT'], 
			[ 'chat', 'chat.chat_from = user.usr_id', 'LEFT'], 
		], $where = NULL, $select = '*', $where_e = NULL, ['created_at', 'DESC'], $start = 0, $tampil = NULL, 'dosen.kd_dosen', $array = 1);

		if ($dosen) {
			foreach ($dosen as $key => $value) {
				$dosen[$key]['flag'] = 0;
				
				if (!is_null($flag)) {
					if ($flag == $value['kd_dosen']) {
						$dosen[$key]['flag'] = 1;
					}
				} else {
					$dosen[0]['flag'] = 1;
				}

				$dosen[$key]['last_chat'] = $this->M_chitchat->lastChat($value['usr_id']);
			}
		}

		return $dosen;
	}

	function getListRecipientDosen($flag = NULL)
	{
		$mhs = $this->M_chitchat->listRecipientForDosen($this->session->userdata('usr_id'));

		if ($mhs) {
			foreach ($mhs as $key => $value) {
				$mhs[$key]['flag'] = 0;
				
				if (!is_null($flag)) {
					if ($flag == $value['nim']) {
						$mhs[$key]['flag'] = 1;
					}
				} else {
					$mhs[0]['flag'] = 1;
				}

				$mhs[$key]['last_chat'] = $this->M_chitchat->lastChat($value['usr_id']);
			}
		}

		return $mhs;
	}

	function send()
	{
		$post = $this->input->post();
		$save = $this->m_global->insert('chat', $post['chat']);

		if ($save) {
			 $this->session->set_flashdata("success", "Berhasil mengirim pesan.");	
		} else {
			 $this->session->set_flashdata("warning", "Gagal mengirim pesan.");	
		}

		redirect('chat/to/'.$post['username']);
	}
}